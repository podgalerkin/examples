%% Header
% @program name: stuart_q.m
% @dependency: -
% @task: Compute convection term q_ijk
% @author: D.M. Luchtenburg
% @see: Technical Report 01/2009 by DML, BRN and MS
% @created: March 2009, DML
function [q] = stuart_q(X,Y,uMean,vMean,uPOD,vPOD)

% Compute convection term q_ijk for Stuart solution
% q_ijk = - Int dxdy [ ( u_j*d/dx(u_k) + v_j*d/dy(u_k) )*u_i + ...
%                      ( u_j*d/dx(v_k) + v_j*d/dy(v_k) )*v_i ]

global W

nModes = size(uPOD,3);
nx = size(uMean,2);
ny = size(uMean,1);
dx = X(1,2)-X(1,1);
dy = Y(2,1)-Y(1,1);

% Init
q = zeros(nModes,nModes+1,nModes+1);
dukdx = zeros(ny,nx); dukdy = zeros(ny,nx);
dvkdx = zeros(ny,nx); dvkdy = zeros(ny,nx);

% Assemble mean and POD modes in one array
u = zeros(ny,nx,nModes+1); v = zeros(ny,nx,nModes+1);
u(:,:,1) = uMean; v(:,:,1) = vMean;
u(:,:,2:nModes+1) = uPOD; v(:,:,2:nModes+1) = vPOD;

% Compute q_ijk
for i = 1:nModes
  for j = 1:nModes+1
    for k = 1:nModes+1
      [dukdx dukdy] = gradient(u(:,:,k),dx,dy);
      [dvkdx dvkdy] = gradient(v(:,:,k),dx,dy);
      q(i,j,k) = -sum(sum(W.*(...
      (u(:,:,j).*dukdx + v(:,:,j).*dukdy).*u(:,:,i+1) + ...
      (u(:,:,j).*dvkdx + v(:,:,j).*dvkdy).*v(:,:,i+1) )));
    end
  end
end
