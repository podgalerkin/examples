%% Header
% @program name: abc_gs.m
% @dependency: -
% @task: Compute GS
% @author: D.M. Luchtenburg
% @see: Technical Report 01/2009 by DML, BRN and MS
% @created: March 2009, DML

function [dadt] = abc_gs(t,a)

% compute GS: dadt = qijk*aj*ak
global qijk

nModes = length(a);
c = [1; a]; % define: a0 = 1
dadt = zeros(nModes,1);

% Sum q_ijk*a_j*a_k
for iMode = 1:nModes
  for j = 1:(nModes+1)
    for k = 1:(nModes+1)
      dadt(iMode) = dadt(iMode) + qijk(iMode,j,k)*c(j)*c(k);
    end
  end
end
