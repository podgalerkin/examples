%% Header
%   @program name: cde_gs.m
%   @dependency: -
%   @task: Compute GS
%   @author: D.M. Luchtenburg
%   @see: Technical Report 01/2009 by DML, BRN and MS
%   @created: March 2009, DML
function [dadt] = cde_gs(t,a)

  % Galerkin system of cde: dadt = L a
  global Lmat
  nModes = length(a);
  dadt = zeros(nModes,1);
  
  % Computed derivative
  dadt = Lmat*a;