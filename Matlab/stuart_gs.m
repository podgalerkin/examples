%% Header
% @program name: stuart_gs.m
% @dependency: -
% @task: Compute GS
% @author: D.M. Luchtenburg
% @see: Technical Report 01/2009 by DML, BRN and MS
% @created: March 2009, DML
function [dadt] = stuart_gs(t,a)

% Galerkin system of Stuart:
% dadt = sum_{j,k=0}^N q_ijk * a_j*a_k
global q

c = [1; a]; % define: a_0 = 1
nModes = length(a);
dadt = zeros(nModes,1); % init
% Computed derivative
for iMode = 1:nModes
  for j = 1:(nModes+1)
    for k = 1:(nModes+1)
      dadt(iMode) = dadt(iMode) + q(iMode,j,k)*c(j)*c(k);
    end
  end
end
