%% Header
%@program name: stuart.m
%@dependency: stuart_q.m, stuart_gs.m
%@task: Compute POD using the snapshot method and
%GM of the convecting Stuart solution
%@author: D.M. Luchtenburg
%@see: Technical Report 01/2009 by DML, BRN and MS
%@created: March 2009, DML

close all; clear all; clc;
%% Globals (defined for integration and integration GS)
global W q
%% Parameters
nModes = 6; % number of computed modes
C = 1.1; % C=1: laminar shear layer, C>1: cat eyes
c = 1;  % convection velocity
nx = 100; % number of grid points in x-direction
ny = 50;  % number of grid points in y-direction
nt = 50;  % number of snapshots
eps1 = 1e-10;  % tolerance for orthonormality
eps2 = 1e-6;   % tolerance for Runge Kutta GS integration
% Figure properties
Quiver.Scale = 2;         % length of vector arrows for field plot
Quiver.LineWidth = 0.1;   % thickness of vector arrows for field plot
XLim = [-2*pi 2*pi];      % x limits for field plot
YLim = [-3 3];            % y limits for field plot
qs = 2;                   % number of points skipped for vector field plot
%% Set up Grid
Lx = 4*pi; % two wave lengths
dx = Lx/(nx-1); x = [-Lx/2:dx:Lx/2];
Ly = 6; % region with constant velocity at edges
dy = Ly/(ny-1); y = [-Ly/2:dy:Ly/2];
[X,Y] = meshgrid(x,y);

%% Create Snapshots
A = sqrt(C^2-1); e = A/C; % vortex strength
dt = 2*pi/(nt-1);
t = [0:dt:2*pi];

u = zeros(ny,nx,nt); v = zeros(ny,nx,nt); % init u,v-vel.
for i = 1:nt
   u(:,:,i) = c + sinh(Y) ./ ( cosh(Y)+e*cos(X-c*t(i)) );
   v(:,:,i) = e*sin(X-c*t(i)) ./ ( cosh(Y)+e*cos(X-c*t(i)) );
end

%% Compute mean corrected snapshots: u" = u - <u>
uMean = mean(u,3); vMean = mean(v,3);
for i = 1:nt
u(:,:,i) = u(:,:,i) - uMean;
v(:,:,i) = v(:,:,i) - vMean;
end

%% Compute weighting matrix W for integration
% use simple midpoint quadrature rule
W = dx*dy*ones(ny,nx); % inner points
W(: , 1) = 0.5*dx*dy*ones(ny,1); % boundary
W(: ,nx) = 0.5*dx*dy*ones(ny,1); % boundary
W( 1, :) = 0.5*dx*dy*ones(1,nx); % boundary
W(ny, :) = 0.5*dx*dy*ones(1,nx); % boundary
W(1 , 1) = 0.25*dx*dy; % corner
W(ny, 1) = 0.25*dx*dy; % corner
W(ny,nx) = 0.25*dx*dy; % corner
W(1 ,nx) = 0.25*dx*dy; % corner

%% Compute correlation matrix C with snapshot method
% C_ij = Int u"(x,t_i).u"(x,t_j) dx
C = zeros(nt,nt); % init
for i = 1:nt
  for j = i:nt
    C(i,j) = sum(sum(W.*( ...
    u(:,:,i).*u(:,:,j) + ...
    v(:,:,i).*v(:,:,j) )));
    C(j,i) = C(i,j); % use symmetry
   end
end
C = C ./ nt;
%% Compute eigenvalues and eigenvectors of C
% Note that the returned eigenvectors are unit vectors,
% i.e. <a_i a_i> = 1
[V,D] = eig(C);
[D,I] = sort(diag(D),"descend");
V = V(:,I);
%% Scale Fourier Coefficients: <a_i a_i> = lambda_i
% a_i(t_j) = V(j,i)*sqrt(Nt*D(i))
FourCoef = zeros(nt,nModes);
for i = 1:nModes
  for j = 1:nt
    ModeFactor = sqrt(nt*D(i));
    FourCoef(j,i) = V(j,i)*ModeFactor;
  end
end

%% Compute POD modes
% u_i = (1/sqrt(Nt*D(i))) * Sum_{j=1}^Nt u"(x,t_j)*V(j,i)
uPOD = zeros(ny,nx,nModes); vPOD = zeros(ny,nx,nModes); % init
for i = 1:nModes
  for j = 1:nt
    uPOD(:,:,i) = uPOD(:,:,i) + V(j,i)*u(:,:,j);
    vPOD(:,:,i) = vPOD(:,:,i) + V(j,i)*v(:,:,j);
  end
  % Normalize
  modeFactor = 1 ./ sqrt(nt*D(i));
  uPOD(:,:,i) = uPOD(:,:,i)*modeFactor;
  vPOD(:,:,i) = vPOD(:,:,i)*modeFactor;
end
%% Check normalization POD modes
Id = zeros(nModes,nModes);
for i=1:nModes
  for j = 1:nModes
    Id(i,j) = sum(sum(W.*( ...
        uPOD(:,:,i).*uPOD(:,:,j) + ...
        vPOD(:,:,i).*vPOD(:,:,j) )));
  end
end
if( abs(norm(eye(nModes)-Id)) > eps1 )
   Id
   disp("Error: POD modes not orthonormal")
end
%% Compute Fourier coefficients by projection (check)
FourCoefProj = zeros(nt,nModes);
for i = 1:nModes
  for j = 1:nt
    FourCoefProj(j,i) = sum(sum(W.*( ...
    u(:,:,j).*uPOD(:,:,i) + ...
    v(:,:,j).*vPOD(:,:,i) )));
  end
end
%% Compute Galerkin system coefficients
q = zeros(nModes,nModes+1,nModes+1);
q = stuart_q(X,Y,uMean,vMean,uPOD,vPOD);
%% Integrate Galerkin system
qijk=q;
aIC = FourCoefProj(1,:); % initial conditions
options = odeset("RelTol",eps2,"AbsTol",eps2*ones(nModes,1));
[tInt,A] = ode45(@stuart_gs,[0 4*pi],aIC,options);

%% Figures
close all

% Plot eigenvalues
figure;
semilogy([1:nt],D./sum(D),"o-");
% axis([1 nt 1.e-3 1])
xlabel("i"); ylabel("\lambda_i / \Sigma \lambda_i")
grid on
title("Normalized eigenvalues")

% Plot comparison direct and projected Fourier coefficients
figure
plot(t,FourCoef(:,1),"r",t,FourCoefProj(:,1),"ro",...
   t,FourCoef(:,2),"g",t,FourCoefProj(:,2),"go")
xlabel("t"); ylabel("a_i")
title("Fourier coefficients (direct -, projection o)")

% Plot Fourier coefficients
for i = 1:2:nModes
  figure
  plot(t,FourCoef(:,i),"b-",...
  t,FourCoef(:,i+1),"r--")
  legend(["a_", num2str(i)],["a_", num2str(i+1)])
  xlabel("t"); ylabel("a_i");
  title("Fourier coefficients")
end

% Plot Phase portraits of Fourier coefficients
figure
for i = 1:nModes/2
  subplot(nModes/2,2,2*i-1)
  plot(FourCoef(:,2*i-1),FourCoef(:,2*i),"-.")
  xlabel(["a_",num2str(2*i-1)]); ylabel(["a_",num2str(2*i)]);
  title("Phase portraits");
  axis("equal")
end

for i = 1:nModes/2-1
  subplot(nModes/2,2,2*i+2)
  plot(FourCoef(:,1),FourCoef(:,2*i+2),"-.")
  xlabel("a_1"); ylabel(["a_",num2str(2*i+2)]);
  title("Phase portraits");
end
% Plot mean flow
figure;
% vorticity
[curlz,cav]= curl(X,Y,uMean,vMean);
pcolor(X,Y,curlz); shading interp
% caxis([-1.12 0]); colorbar;
hold on;
% vortor plot
Xq = X(1:qs:end, 1:qs:end);
Yq = Y(1:qs:end, 1:qs:end);
Uq = uPOD(1:qs:end, 1:qs:end,i);
Vq = vPOD(1:qs:end, 1:qs:end,i);
quiver(Xq, Yq, Uq, Vq, Quiver.Scale, "k-", "LineWidth", Quiver.LineWidth);
axis equal;
set(gca, "XLim", XLim);
set(gca, "YLim", YLim);
xlabel("x"); ylabel("y");
title(["mean flow"])

% Plot POD modes
for i = 1:nModes
  figure;
  % vorticity
  [curlz,cav]= curl(X,Y,uPOD(:,:,i),vPOD(:,:,i));
  pcolor(X,Y,curlz); shading interp
  caxis([-1.2 1.2]); % colorbar;
  hold on;
  % vortor plot
  Xq = X(1:qs:end, 1:qs:end);
  Yq = Y(1:qs:end, 1:qs:end);
  Uq = uPOD(1:qs:end, 1:qs:end,i);
  Vq = vPOD(1:qs:end, 1:qs:end,i);
  quiver(Xq, Yq, Uq, Vq, Quiver.Scale, "k-", "LineWidth", Quiver.LineWidth);
  axis equal;
  set(gca, "XLim", XLim);
  set(gca, "YLim", YLim);
  xlabel("x"); ylabel("y");
  title(["POD mode ",num2str(i)])
end

% Plot q_i0k (convection)
figure
image(squeeze(q(:,1,:)),"CDataMapping","scaled")
cm = max(abs(min(min(squeeze(q(:,1,:))))),max(max(squeeze(q(:,1,:)))));
caxis([-cm cm])
xlabel("k"); ylabel("i");
set(gca, "XTickLabel", [0:nModes]);
colorbar
xlabel("k"); ylabel("i");
title("q_{i0k} (->convection)")
% Plot comparison projected and GS integrated Fourier coefficients
figure
plot(tInt,A,"--x")
hold on
plot(t,FourCoef,"-")
xlabel("t"); ylabel("a_i")
title("Comparison of projected a_i (-) and GS integrated a_i (--x)")
