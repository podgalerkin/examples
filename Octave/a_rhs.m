% Right hand side for ODE 45 timestepper
% for reduced order model FFT based nonlinear 
% Schrödinger solver
% by J.N. Kutz
% Original codes and lectures are avalable at
% http://faculty.washington.edu/kutz/rom/page1/page5/rom.html

function rhs=a_rhs(t,a,dummy,phi,Lr)

rhs= Lr*a + i*phi'*( (abs(phi*a).^2).*(phi*a) );