% A program to solve the Convection diffusion equation using a
% second order implicit midpoint rule method
% u_{t}+u_x =\epsilon u_{xx}
% The first solver uses the Fast Fourier transform
% The second solver uses the Reduced Basis Method and uses
% Example codes by
% a) J.N. Kutz for the Nonlinear Schrödinger equation
% b) C.I. Gheorghiu for the Sinc method
% as example templates
clear all; format compact; format short;
set(0,'defaultaxesfontsize',30,'defaultaxeslinewidth',.7,...
    'defaultlinelinewidth',6,'defaultpatchlinewidth',3.7,...
    'defaultaxesfontweight','bold')

Nx=400;  % order of approximation
dx=0.5;  % spacing of SiC
Nt=1000; % Number of timesteps
dt=0.1; % Timestep
nu=0.5; % Diffusion
r=15;  % rank of projection
[x,D]=sincdif(Nx,2,dx); D1=D(:,:,1); D2=D(:,:,2); % SiC diffrentiation
uu=exp(-(x+75).^2).*cos(x); 
t=0; tdata(1)=t;

LL=-D1+nu*D2;
% Solve using Implicit Midpoint rule
plotnum=1;
% solve pde and plot results
usol(1,:)=uu;
tic
for n =1:Nt+1
    uu=expm(LL*dt)*uu; 
    usol(n+1,:)=uu;
    t=n*dt;
    tdata(n+1)=t;
end
toc
figure(1);
surfl(x,tdata,usol); shading interp, colormap(hot);

%% SVD 

X=usol.';
[u,s,v]=svd(X);
figure(2)
plot(diag(s)/sum(diag(s)),'ko','Linewidth',[2])

figure(3)
plot(real(u(:,1:r+1)),'Linewidth',[2])

phi=u(:,1:r);  % Phi_r POD modes

for j=1:r
  phixx(:,j)=D2*phi(:,j); % second derivatives
  phix(:,j)=D1*phi(:,j);  % first derivatives
  a0(j)=(exp(-(x+75).^2).*cos(x))'*conj(phi(:,j));  % projection of initial conditions
end
D2r=phi'*phixx;  % Low-rank approximation of second derivative operator
D1r=phi'*phix; % Low-rank approximation of first derivative operator


% solve pde and plot results
asol(1,:)=a0;
aa=a0';
aanew=aa;
LLr=-D1r+nu*D2r;
tic
for n =1:Nt+1
    aa=expm(LLr*dt)*aa; 
    asol(n+1,:)=aa;
end
toc

us=zeros(Nx,length(tdata));
for j=1:length(tdata)
    for jj=1:r
       us(:,j)=us(:,j) + asol(j,jj)*phi(:,jj);  % r-rank reconstruction
    end
end
figure(4)
surfl(x,tdata,abs(us.')); shading interp, colormap(hot);


