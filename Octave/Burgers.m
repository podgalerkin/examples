% A program to solve the 1D Burgers equation using a
% second order implicit midpoint rule method
% u_{t}+(u^2)_x =\epsilon u_{xx}
% The first solver uses the Fast Fourier transform
% The second solver uses the Reduced Basis Method and uses
% Example codes by J.N. Kutz for the Nonlinear Schrödinger equation
% as example templates
% See also 
% K. Veroy, C. Prud’homme, and A. T. Patera. 
% "Reduced-basis approximation of the viscous Burgers equation: 
% Rigorous a posteriori error bounds."
% Comptes Rendus de l’Académie des Sciences, Série I, 337(9):619–624, 2003.
% http://dx.doi.org:10.1016/j.crma.2003.09.023

clear all; format compact; format short;
set(0,'defaultaxesfontsize',30,'defaultaxeslinewidth',.7,...
    'defaultlinelinewidth',6,'defaultpatchlinewidth',3.7,...
    'defaultaxesfontweight','bold')

% set up grid
tic
Lx = 8;        % period 2*pi*L
Nx = 256;      % number of harmonics
Nt = 10000;    % number of time slices
tol=0.1^12;    % tolerance for fixed point iterations
epsilon=0.1;   % dissipation
dt = 10.0/Nt;   % time step
r=15;  % rank of projection

t=0; tdata(1)=t;

% initialise variables
x = (2*pi/Nx)*(-Nx/2:Nx/2 -1)'*Lx;   % x coordinate
kx = 1i*[0:Nx/2-1 0 -Nx/2+1:-1]'/Lx; % wave vector

% initial conditions
uu = sqrt(2)*sech(x);
vv=fft(uu,[],1);

plotnum=1;
% solve pde and plot results
usol(1,:)=uu;
tic
for n =1:Nt+1
    uunew=uu;
    chg=1;
    while(chg>tol)
      uutemp=uunew;
      nonlin=0.5*(0.5*(uunew+uu)).^2;
      nonlinhat=fft(nonlin,[],1);
      vvnew=vv+dt*( 0.5*epsilon*kx.*kx.*vv-kx.*nonlinhat)...
            ./(1-dt*epsilon*0.5*kx.*kx);
      uunew=ifft(vvnew,[],1);
      chg=sum((abs(uunew-uutemp).^2)/Nx);
    endwhile
    t=n*dt;
    tdata(n+1)=t;
    usol(n+1,:)=real(uunew);
    % update old terms
    vv=vvnew;
    uu=uunew;
end
toc
figure(1);
surfl(x,tdata,usol); shading interp, colormap(hot);

%% SVD 

X=usol.';
[u,s,v]=svd(X);
figure(2)
plot(diag(s)/sum(diag(s)),'ko','Linewidth',[2])

figure(3)
plot(real(u(:,1:r+1)),'Linewidth',[2])

phi=u(:,1:r);  % Phi_r POD modes

for j=1:r
  phixx(:,j)=ifft((kx.^2).*fft(phi(:,j))); % second derivatives
  phix(:,j)=ifft(kx.*fft(phi(:,j))); % first derivatives
  a0(j)=sqrt(2)*sech(x)'*conj(phi(:,j));  % projection of initial conditions
end
Lr=phi'*phixx;  % Low-rank approximation of second derivative operator
Nr=phi'*phix; % Low-rank approximation of first derivative operator

% solve pde and plot results
asol(1,:)=a0;
aa=a0';
aanew=aa;
A=(eye(size(Lr))-dt*0.5*epsilon*Lr);
[L, U, P] = lu (A);
tic
for n =1:Nt+1
    chg=1;
    while(chg>tol)
      aatemp=aanew;
      rhs=P'*(aa+dt*(0.5*epsilon*Lr*aa-...
                 phi'*((0.5*phi*(aanew+aa)).*(0.5*phix*(aanew+aa)))));
      temp=linsolve(U,rhs); 
      aanew=linsolve(L,temp); 
      chg=sum((abs(aanew-aatemp).^2)/r);
    endwhile
    asol(n+1,:)=aanew;
    % update old terms
    aa=aanew;
end
toc

us=zeros(Nx,length(tdata));
for j=1:length(tdata)
    for jj=1:r
       us(:,j)=us(:,j) + asol(j,jj)*phi(:,jj);  % r-rank reconstruction
    end
end
figure(4)
surfl(x,tdata,abs(us.')); shading interp, colormap(hot);


