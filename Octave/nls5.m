% A Matlab/Octave reduced order model solver for the
% Nonlinear Schrödinger equation 
% Original by J.N. Kutz available at:
% http://faculty.washington.edu/kutz/rom/page1/page5/rom.html
% Modified by B.K. Muite to not use ODE45

clear all; close all; clc

L=30; n=256; nt=100; 
x2=linspace(-L/2,L/2,n+1); x=x2(1:n);
k=(2*pi/L)*[0:n/2-1 -n/2:-1].';
t=linspace(0,0.1*pi,nt);
dt=t(2);
r=8;  % rank of projection

order = "2nd"; % order of method, 2nd, 4th or 6th

% Splitting constants from 
% http://www.asc.tuwien.ac.at/~winfried/splitting/index.php

switch (order)
% 2nd order Strang splitting constants 
  case { "2nd" }
asplit=[0.5,
0.5];
bsplit=[1.00000000000,
 0.000000000000000000]; 
 stages=2;
% 4th order Yosida splitting constants 
  case { "4th" }
asplit=[0.675603595979828817,
-0.175603595979828817,
-0.175603595979828817,
 0.675603595979828817];
bsplit=[1.351207191959657634,
-1.702414383919315268,
 1.351207191959657634,
 0.000000000000000000]; 
 stages=4;
 otherwise
% 6th oder Yosida splitting constants
asplit=[0.392256805238778632,
 0.510043411918457699,
-0.471053385409756437,
 0.068753168252520106,
 0.068753168252520106,
-0.471053385409756437,
 0.510043411918457699,
 0.392256805238778632];
bsplit=[0.784513610477557264,
 0.235573213359358134,
-1.177679984178871007,
 1.315186320683911218,
-1.177679984178871007,
 0.235573213359358134,
 0.784513610477557264,
 0.000000000000000000];
 stages=8;
endswitch
 u=2*sech(x);
ut=fft(u);
% solve using splitting
utsol(1,:)=ut;
tic
for j=1:nt
  for jj=1:(stages-1)
    ut=exp(asplit(jj)*-1i*0.5*dt*(k').^2).*ut;
    temp=ifft(ut);
    pot=temp.*conj(temp);
    temp=exp(1i*dt*bsplit(jj)*pot).*temp;
    ut=fft(temp);
  end
  ut=exp(asplit(stages)*-1i*0.5*dt*(k').^2).*ut;
  utsol(j+1,:)=ut;
end
toc
for j=1:length(t)
   usol(j,:)=ifft(utsol(j,:));
end
figure(1);
surfl(x,t,abs(usol)); shading interp, colormap(hot);

%% SVD 

X=usol.';
[u,s,v]=svd(X);
figure(2)
plot(diag(s)/sum(diag(s)),'ko','Linewidth',[2])

figure(3)
plot(real(u(:,1:3)),'Linewidth',[2])

phi=u(:,1:r);  % Phi_r POD modes

for j=1:r
  phixx(:,j)=  -ifft((k.^2).*fft(phi(:,j))); % second derivatives
end
a0=2*phi'*(sech(x))';  % projection of initial conditions
Lr= (i/2)*phi'*phixx;  % Low-rank approximation of linear term

% solve using splitting
asol(1,:)=a0;
a=a0;

tic
for j=1:nt
    for jj=1:(stages-1)
      a=expm(asplit(jj)*Lr*dt)*a;
      a=phi'*(exp(bsplit(jj)*1i*dt*(phi*a).*conj(phi*a)).*(phi*a));
    end
    a=expm(asplit(stages)*Lr*dt)*a;
    asol(j+1,:)=a;
end
toc
  
us=zeros(n,length(t));
for j=1:length(t)
    for jj=1:r
       us(:,j)=us(:,j) + asol(j,jj)*phi(:,jj);  % r-rank reconstruction
    end
end
figure(4);
plot(x,abs(us(:,1)'-usol(1,:)))
figure(5);
plot(x,real(us(:,1)),'r-',x,imag(us(:,1)),'g-',...
     x,real(usol(1,:)),'b:',x,imag(usol(1,:)),'y:');

figure(6);
surfl(x,t,abs(us.')); shading interp, colormap(hot);

figure(7);
surfl(x,t,abs(us.'-usol)); shading interp, colormap(hot);
