% Right hand side for ODE 45 timestepper
% for FFT based nonlinear Schrödinger solver
% by J.N. Kutz
% Original codes and lectures are avalable at
% http://faculty.washington.edu/kutz/rom/page1/page5/rom.html

function rhs=nls_rhs(t,ut,dummy,k)
u=ifft(ut);
rhs=-(i/2)*(k.^2).*ut + i*fft( (abs(u).^2).*u );