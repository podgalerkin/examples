% A Matlab/Octave reduced order model solver for the
% Nonlinear Schrödinger equation 
% Original by J.N. Kutz available at:
% http://faculty.washington.edu/kutz/rom/page1/page5/rom.html
% Modified by B.K. Muite to not use ODE45

clear all; close all; clc

L=30; n=256; nt=501; tol=0.1^12;
x2=linspace(-L/2,L/2,n+1); x=x2(1:n);
k=(2*pi/L)*[0:n/2-1 -n/2:-1].';
t=linspace(0,2*pi,nt);
dt=t(2);

u=2*sech(x);
ut=fft(u);
% solve using Strang Splitting
utsol(1,:)=ut;
for j=1:nt
  ut=exp(0.5*1i*0.5*dt*(k').^2).*ut;
  temp=ifft(ut);
  pot=temp.*conj(temp);
  temp=exp(-1i*dt*pot).*temp;
  ut=fft(temp);
  ut=exp(0.5*1i*0.5*dt*(k').^2).*ut;
  utsol(j+1,:)=ut;
end

for j=1:length(t)
   usol(j,:)=ifft(utsol(j,:));
end

surfl(x,t,abs(usol)); shading interp, colormap(hot);


%% SVD 

X=usol.';
[u,s,v]=svd(X);
figure(2)
plot(diag(s)/sum(diag(s)),'ko','Linewidth',[2])

figure(3)
plot(real(u(:,1:3)),'Linewidth',[2])

r=4;  % rank of projection
phi=u(:,1:r);  % Phi_r POD modes

for j=1:r
  phixx(:,j)=  -ifft((k.^2).*fft(phi(:,j))); % second derivatives
  a0(j)= 2*sech(x)*conj(phi(:,j));  % projection of initial conditions
end
Lr= (i/2)* phi'*phixx;  % Low-rank approximation of linear term

% solve using IMR
% splitting error seems to become too large
asol(1,:)=a0';
a=a0';

for j=1:nt
  chg=1;
  aold=a;
  while(chg>tol)
    atemp=a;
    abar=0.5*(a+aold);
    a=aold+dt*(Lr*abar + i*phi'*( (abs(phi*abar).^2).*(phi*abar) ));
    chg=sqrt(sum(abs(a-atemp).^2));
  endwhile
  asol(j+1,:)=a;
end

  
us=zeros(n,length(t));
for j=1:length(t)
    for jj=1:r
       us(:,j)=us(:,j) + asol(j,jj)*phi(:,jj);  % r-rank reconstruction
    end
end

surfl(x,t,abs(us.')); shading interp, colormap(hot);
