% A program to solve the 1D cubic Klein Gordon equation using a
% second order explicit method, Crank-Nicolson
% u_{tt}-u_{xx}+u=u^3
% The first solver uses the Fast Fourier transform and is a modified
% version from 
% https://en.wikibooks.org/wiki/Parallel_Spectral_Numerical_Methods/The_Klein-Gordon_Equation
% The second solver uses the Reduced Basis Method and uses
% example codes by J.N. Kutz for the Nonlinear Schrödinger equation
% as example templates
% Original author B.K. Muite

clear all; format compact; format short;
set(0,'defaultaxesfontsize',30,'defaultaxeslinewidth',.7,...
    'defaultlinelinewidth',6,'defaultpatchlinewidth',3.7,...
    'defaultaxesfontweight','bold')

% set up grid
tic
Lx = 8; % period 2*pi*L
Nx = 256; % number of harmonics
Nt = 2000; % number of time slices
c=0.5; % wave speed
dt = 1.0/Nt; % time step

Es = 1.0; % focusing (+1) or defocusing (-1) parameter
t=0; tdata(1)=t;

% initialise variables
x = (2*pi/Nx)*(-Nx/2:Nx/2 -1)'*Lx; % x coordinate
kx = 1i*[0:Nx/2-1 0 -Nx/2+1:-1]'/Lx; % wave vector

% initial conditions
uu = sqrt(2)*sech((x-c*t)/sqrt(1-c^2));
uuexact= sqrt(2)*sech((x-c*t)/sqrt(1-c^2));
uuold=sqrt(2)*sech((x+c*dt)/sqrt(1-c^2));
vv=fft(uu,[],1);
vvold=fft(uuold,[],1);

plotnum=1;
% solve pde and plot results
usol(1,:)=uu;
tic
for n =1:Nt+1
    nonlin=uu.^3;
    nonlinhat=fft(nonlin,[],1);
    vvnew=( dt*dt*(kx.*kx -1).*vv...
           +(2*vv-vvold) +dt*dt*Es*nonlinhat);
    uunew=ifft(vvnew,[],1);
    t=n*dt;
    tdata(n+1)=t;
    usol(n+1,:)=real(uunew);
    % update old terms
    vvold=vv;
    vv=vvnew;
    uuold=uu;
    uu=uunew;
end
toc
figure(1);
surfl(x,tdata,usol); shading interp, colormap(hot);

%% SVD 

X=usol.';
[u,s,v]=svd(X);
figure(2)
plot(diag(s)/sum(diag(s)),'ko','Linewidth',[2])

figure(3)
plot(real(u(:,1:3)),'Linewidth',[2])

r=4;  % rank of projection
phi=u(:,1:r);  % Phi_r POD modes

for j=1:r
  phixx(:,j)=ifft((kx.^2).*fft(phi(:,j))); % second derivatives
  a0(j)=sqrt(2)*sech((x-c*0)/sqrt(1-c^2))'*conj(phi(:,j));  % projection of initial conditions
  am1(j)=sqrt(2)*sech((x+c*dt)/sqrt(1-c^2))'*conj(phi(:,j)); % projection of initial conditions
end
Lr=phi'*phixx;  % Low-rank approximation of linear term

% solve pde and plot results
asol(1,:)=a0;
aaold=am1';
aa=a0';
tic
for n =1:Nt+1
    aanew=2*aa-aaold+(dt*dt)*(Lr*aa-aa+Es*phi'*((phi*aa).^3)); 
    asol(n+1,:)=aanew;
    % update old terms
    aaold=aa;
    aa=aanew;
end
toc

us=zeros(Nx,length(tdata));
for j=1:length(tdata)
    for jj=1:r
       us(:,j)=us(:,j) + asol(j,jj)*phi(:,jj);  % r-rank reconstruction
    end
end

surfl(x,tdata,abs(us.')); shading interp, colormap(hot);


