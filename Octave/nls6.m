% A Matlab/Octave reduced order model solver for the
% Nonlinear Schrödinger equation 
% The second solver uses the Reduced Basis Method and uses
% Example codes by
% a) J.N. Kutz for the Nonlinear Schrödinger equation
% b) C.I. Gheorghiu for the Sinc method
% as example templates
clear all; format compact; format short;
set(0,'defaultaxesfontsize',30,'defaultaxeslinewidth',.7,...
    'defaultlinelinewidth',6,'defaultpatchlinewidth',3.7,...
    'defaultaxesfontweight','bold')

Nx=400;  % order of approximation
dx=0.1;  % spacing of SiC
Nt=200; % Number of timesteps
dt=0.01; % Timestep
r=15;  % rank of projection
[x,D]=sincdif(Nx,2,dx); D1=D(:,:,1); D2=D(:,:,2); % SiC diffrentiation
uu=2*sech(x); 
t=0; tdata(1)=t;

order = "4th"; % order of method, 2nd, 4th or 6th

% Splitting constants from 
% http://www.asc.tuwien.ac.at/~winfried/splitting/index.php

switch (order)
% 2nd order Strang splitting constants 
  case { "2nd" }
asplit=[0.5,
0.5];
bsplit=[1.00000000000,
 0.000000000000000000]; 
 stages=2;
% 4th order Yosida splitting constants 
  case { "4th" }
asplit=[0.675603595979828817,
-0.175603595979828817,
-0.175603595979828817,
 0.675603595979828817];
bsplit=[1.351207191959657634,
-1.702414383919315268,
 1.351207191959657634,
 0.000000000000000000]; 
 stages=4;
 otherwise
% 6th oder Yosida splitting constants
asplit=[0.392256805238778632,
 0.510043411918457699,
-0.471053385409756437,
 0.068753168252520106,
 0.068753168252520106,
-0.471053385409756437,
 0.510043411918457699,
 0.392256805238778632];
bsplit=[0.784513610477557264,
 0.235573213359358134,
-1.177679984178871007,
 1.315186320683911218,
-1.177679984178871007,
 0.235573213359358134,
 0.784513610477557264,
 0.000000000000000000];
 stages=8;
endswitch
% solve using splitting
uusol(1,:)=uu;
LL=-1i*0.5*D2;
plotnum=1;
tic
for n =1:Nt
  for jj=1:(stages-1)
    uu=expm(asplit(jj)*dt*LL)*uu;
    pot=uu.*conj(uu);
    uu=exp(-1i*dt*bsplit(jj)*pot).*uu;
  end
  uu=expm(asplit(stages)*dt*LL)*uu;
  uusol(n+1,:)=uu;
  t=n*dt;
  tdata(n+1)=t;
end
toc
figure(1);
surfl(x,tdata,abs(uusol)); shading interp, colormap(hot);

%% SVD 

X=uusol.';
[u,s,v]=svd(X);
figure(2)
plot(diag(s)/sum(diag(s)),'ko','Linewidth',[2])

figure(3)
plot(real(u(:,1:r+1)),'Linewidth',[2])

phi=u(:,1:r);  % Phi_r POD modes

for j=1:r
  phixx(:,j)=D2*phi(:,j); % second derivatives
  phix(:,j)=D1*phi(:,j);  % first derivatives
  a0(j)=(2*sech(x))'*conj(phi(:,j));  % projection of initial conditions
end
D2r=phi'*phixx;  % Low-rank approximation of second derivative operator
D1r=phi'*phix; % Low-rank approximation of first derivative operator

Lr= -0.5*(1i)*phi'*phixx;  % Low-rank approximation of linear term

% solve using splitting
asol(1,:)=a0;
a=a0';
tic
for j=1:Nt
    for jj=1:(stages-1)
      a=expm(asplit(jj)*Lr*dt)*a;
      a=phi'*(exp(bsplit(jj)*-1i*dt*(phi*a).*conj(phi*a)).*(phi*a));
    end
    a=expm(asplit(stages)*Lr*dt)*a;
    asol(j+1,:)=a;
end
toc
  
us=zeros(Nx,length(tdata));
for j=1:length(tdata)
    for jj=1:r
       us(:,j)=us(:,j) + asol(j,jj)*phi(:,jj);  % r-rank reconstruction
    end
end
figure(4);
plot(x,abs(us(:,1)'-uusol(1,:)))
figure(5);
plot(x,real(us(:,1)),'r-',x,imag(us(:,1)),'g-',...
     x,real(uusol(1,:)),'b:',x,imag(uusol(1,:)),'y:');

figure(6);
surfl(x,tdata,abs(us.')); shading interp, colormap(hot);

figure(7);
surfl(x,tdata,abs(us.'-uusol)); shading interp, colormap(hot);