# Examples

Example programs from:
"An introduction to the POD Galerkin method for fluid flows with analytical examples and MATLAB source codes" 
by
[D.M. Luchtenburg](https://cooper.edu/academics/people/dirk-martin-luchtenburg), [B.R. Noack](http://berndnoack.com) & [M. Schlegel](https://www.researchgate.net/profile/Michael_Schlegel)

Berlin Institute of Technology
Department of Fluid Dynamics and Engineering Acoustics
Chair in Reduced-Order Modelling for Flow Control
Technical Report 01/2009

The report is available [here](http://berndnoack.com/publications/Luchtenburg2009romfc.PDF)

Additional examples are also taken from Reduced Order Modeling by [J.N. Kutz](http://faculty.washington.edu/kutz)
which can be found [here](http://faculty.washington.edu/kutz/rom/page1/page5/rom.html)

Further codes used from
* [C.I. Gherghiou](http://www.ictp.acad.ro/gheorghiu)  [Spectral Collocation Solutions to Problems on Unbounded Domains](http://www.agir.ro/carte/spectral-collocation-solutions-to-problems-on-unbounded-domains-123087.html), Casa Cărtii de Știintă, Cluj-Napoca, 2018, pp. 151, ISBN 978-606-17-1272-4 
* [J. Shen](https://www.math.purdue.edu/~shen/), [T. Tang](http://www.sustc.edu.cn/en/faculty_54/f/TANG_Tao), [L.-L. Wang](http://www.ntu.edu.sg/home/lilian/index.htm)  [SPECTRAL METHODS: Algorithms, Analysis and Applications](http://dx.doi.org/10.1007/978-3-540-71041-7)  [codes](http://www.ntu.edu.sg/home/lilian/book.htm)
* [L.N. Trefethen](http://people.maths.ox.ac.uk/trefethen/index.html) [Spectral Methods in MATLAB](https://doi.org/10.1137/1.9780898719598) [codes](http://people.maths.ox.ac.uk/trefethen/spectral.html)
* [J.A.C. Weideman](http://appliedmaths.sun.ac.za/~weideman/) - [A Matlab Differentiation Suite](http://appliedmaths.sun.ac.za/~weideman/research/differ.html)

